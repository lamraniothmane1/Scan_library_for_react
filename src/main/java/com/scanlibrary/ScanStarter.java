package com.scanlibrary;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;

import static com.scanlibrary.Utils.REQUEST_CODE;

/**
 * Created by Lamrani on 07/02/2018.
 */

public class ScanStarter {

    private Activity activity;

    public ScanStarter(Activity activity){
        this.activity = activity;
    }

    /**
     * Start Scan activity
     */
    public void start(){
        // create new Intent to start ScanActivity
        Intent intent = new Intent(activity, ScanActivity.class);
        // **** start the scan activity ****
        activity.startActivityForResult(intent, REQUEST_CODE);
    }


    /**
     * Encode to base 64
     */
    public static String encodeTobase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();

        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);

        return imageEncoded;
    }

}
